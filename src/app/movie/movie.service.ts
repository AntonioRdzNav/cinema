import { Injectable } from '@angular/core';
import { MovieModel } from './movie.model';

@Injectable()
export class MovieService {
    private movies: MovieModel[];

    constructor() {
        this.movies = [];
        const matrix = new MovieModel();
        matrix.posterUrl = 'http://t0.gstatic.com/images?q=tbn:ANd9GcQq3pIz-aKgkmYX1dJ-EL-AlHSPcOO7wdqRIJ5gJy9qNinXpmle';
        matrix.name = 'The Matrix';
        matrix.year = 1999;
        matrix.director = 'The Wachowski Brothers';
        matrix.description = 'A computer hacker learns from mysterious rebels about the true nature of his reality and his role in the war against its controllers.';
        this.movies.push(matrix);

        const akira = new MovieModel();
        akira.posterUrl = 'https://upload.wikimedia.org/wikipedia/en/thumb/5/5d/AKIRA_%281988_poster%29.jpg/220px-AKIRA_%281988_poster%29.jpg';
        akira.name = 'Akira';
        akira.year = 1988;
        akira.director = 'Katsuhiro Otomo';
        akira.description = 'A secret military project endangers Neo-Tokyo when it turns a biker gang member into a rampaging psychic psychopath that only two teenagers and a group of psychics can stop.';
        this.movies.push(akira);
    }

    public getMovies() {
        return this.movies.slice();
    }

    public getMovieByIndex(index: number): MovieModel {
        return this.movies.slice()[index];
    }
}
