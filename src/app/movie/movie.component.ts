import { Component, Input } from '@angular/core';
import { MovieModel } from './movie.model';

@Component({
    templateUrl: 'movie.component.html',
    selector: 'app-movie'
})
export class MovieComponent {
    @Input() movie: MovieModel;

}
