import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScreenListComponent } from './screen/screen-list.compoment';
import { ScreenComponent } from './screen/screen.component';

const appRoutes: Routes = [
    { path: '', component: ScreenListComponent },
    { path: ':index/screen', component: ScreenComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
