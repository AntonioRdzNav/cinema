import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { MovieModel } from '../movie/movie.model';
import { MovieService } from '../movie/movie.service';
import { ScreenModeEnum } from './screen-mode.enum';

@Component({
    selector: 'app-screen',
    templateUrl: 'screen.component.html'
})
export class ScreenComponent implements OnInit {
    movie: MovieModel;
    @Input() index: number;
    mode: ScreenModeEnum;
    screenModeEnum = ScreenModeEnum;

    constructor(
        private movieService: MovieService,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.route.params
        .subscribe((params: Params) => {
            this.index = +params['index'];
            if (params['index'] === undefined) {
                this.mode = ScreenModeEnum.List;
            } else {
                this.mode = ScreenModeEnum.Single;
            }

        });
        this.movie = new MovieModel();
    }

    ngOnInit() {
        this.movie = this.movieService.getMovieByIndex(this.index);
    }

    goHome() {
        this.router.navigate(['/']);
    }
}
