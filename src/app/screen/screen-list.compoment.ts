import { Component } from '@angular/core';
import { MovieModel } from '../movie/movie.model';
import { MovieService } from '../movie/movie.service';

@Component({
    selector: 'app-screen-list',
    template: `
    <app-screen *ngFor="let movie of movies; let i = index" [index]="i" ></app-screen>
    `
})
export class ScreenListComponent {
    movies: MovieModel[];

    constructor(
      private movieService: MovieService
    ) {
      this.movies = this.movieService.getMovies();
    }

}
